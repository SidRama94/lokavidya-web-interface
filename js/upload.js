app.controller("uploadCtrl", function($scope, $http, $document) {
	$(function(){
		var form = $(".upload-form");

		form.css({
			opacity: 1,
			"-webkit-transform": "scale(1)",
			"transform": "scale(1)",
			"-webkit-transition": ".5s",
			"transition": ".5s"
		});
	});
	var token = document.getElementById("token");
	var names = [];
	console.log("Token: "+token);
	var categoryId = [];
	var count =0;
	var settings = {
		"url": "http://ruralict.cse.iitb.ac.in/lokavidya/api/categorys",
		"method": "GET",
		"headers": {
    //"x-auth-token": "user:1461314239038:acdab31fce4a8ed81ab5fea3d853caaf",
    "x-auth-token": "user:1463462842841:94458b80a8879fdd758d13d0f605338b",
    "cache-control": "no-cache",
    "postman-token": "5c511468-4e16-9711-a4c9-eded372bb7ef"
}
}
var url = "http://ruralict.cse.iitb.ac.in/lokavidya/api/categorys";

$http.get(url,settings).success( function(response) {
	var data = response;
	for (var i =0; i < data.length;++i) {
		names[count] = data[i].name;
		categoryId[count] = data[i].id;
		++count;

	}
	$scope.category_names = names;
	$scope.category_id = categoryId;
});


$scope.uploadProject = function() {


	var form = new FormData();
	form.append("tutorialName", $scope.form.tutorial);
	form.append("tutorialDescription", $scope.form.description);
	form.append("categoryId", parseInt($scope.form.category),10);
	form.append("keywords", $scope.form.keyword);
	form.append("language", $scope.form.language);
	form.append("file", document.getElementById('file-input').files[0]);
	console.log("data: "+document.getElementById('file-input').files[0]);
	var settings = {
		"async": true,
		"crossDomain": true,
		"url": "http://ruralict.cse.iitb.ac.in/lokavidya/api/tutorials/upload",
		"method": "POST",
		"headers": {
			"x-auth-token": "user:1463462842841:94458b80a8879fdd758d13d0f605338b",
  //"x-auth-token": token,
  "cache-control": "no-cache",
  "postman-token": "a4d2787f-adea-d7b2-8a54-a36fb9c07d76"
},
"processData": false,
"contentType": false,
"mimeType": "multipart/form-data",
"data": form
}

$.ajax(settings).done(function (response) {
	console.log(response);
});

}; 

});