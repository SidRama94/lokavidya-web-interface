app.factory('video',function ($document) {
 // var videoElement = $document[0].createElement('video'); // <-- Magic trick here
  var videoElement = $document[0].getElementById('video-view');
  var token = document.getElementById('token');
  return {
    videoElement: videoElement,

    play: function(filename) {
        videoElement.src = filename;
        //videoElement.play();     //  <-- Thats all you need
    }
   }
});
app.controller("viewCtrl", function($scope, $http, video) {
	var vm = this;
	vm.mydata = [];
	$scope.items=[];
	var links = [];
	var names = [];
	var arr = [];
	var relatedObj = [];
	var related = [];

	$scope.videoSelect = function(videoPath,id) {
		relatedObj = [];
		related = [];
		$scope.related = [];
        video.play("http://"+videoPath);    //     <---  Thats All You Need !
        var settingsRelated = {
          "url": "http://ruralict.cse.iitb.ac.in/lokavidya/api/tutorials/"+id,
          "method": "GET",
          "headers": {
            "x-auth-token": "user:1463462842841:94458b80a8879fdd758d13d0f605338b",
            "cache-control": "no-cache",
            "postman-token": "5c511468-4e16-9711-a4c9-eded372bb7ef"
         }
        }
      var url = "http://ruralict.cse.iitb.ac.in/lokavidya/api/tutorials/"+id;
      $http.get(url,settingsRelated).success( function(response) {
      var count =0;
      console.log(response);
      console.log("success");
      var dataRel = response;
      relatedObj = JSON.parse(response.referenceResourceLink);
     // relatedObj = response.referenceResourceLink;
     for (var x =0; x < relatedObj.length;++x){
      var obj = {};
      obj.link = relatedObj[x].url;
      obj.name = relatedObj[x].name;
      obj.id= relatedObj[x].videoId; 
      related[count] = obj;
      ++count;
     }
      console.log("size: "+ relatedObj.length.toString());
      $scope.related = related;
    });
  }


	var settings = {

		"url": "http://ruralict.cse.iitb.ac.in/lokavidya/api/tutorials",
		"method": "GET",
		"headers": {
  //  "x-auth-token": "user:1461314239038:acdab31fce4a8ed81ab5fea3d853caaf",
  "x-auth-token": "user:1463462842841:94458b80a8879fdd758d13d0f605338b",
  "cache-control": "no-cache",
  "postman-token": "5c511468-4e16-9711-a4c9-eded372bb7ef"
}
}
var url = "http://ruralict.cse.iitb.ac.in/lokavidya/api/tutorials";
$http.get(url,settings).success( function(response) {
	var count =0;
	console.log(response);
	console.log("success");
	var data = response;
	for (var i =0; i < data.length;++i) {
		var obj = {};
		var videoObj = data[i].externalVideo;
		if (videoObj != null) {
			obj.link = videoObj.httpurl;
			obj.name = data[i].name;
			obj.id = data[i].id;
			arr[count] =obj;
			links[count] = videoObj.httpurl;
			names[count] = data[i].name;
			++count;
			console.log("Video URL: "+ obj.link);
		}
	}
	$scope.items = links;
	$scope.names = names;
	$scope.arr = arr;
	console.log("http://"+links[0]);


});


});