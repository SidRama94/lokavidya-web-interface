app.controller("feedbackCtrl", function($scope, $http) {
$(function(){
		var form = $(".feedback-form");

		form.css({
			opacity: 1,
			"-webkit-transform": "scale(1)",
			"transform": "scale(1)",
			"-webkit-transition": ".5s",
			"transition": ".5s"
		});
	});

$scope.submitFeedback = function() {
	var x = {
		'category':$scope.feedback.type, 
	'messagedescription':$scope.feedback.desc
	};
	var json = JSON.stringify(x);
	var url = "http://ruralict.cse.iitb.ac.in/lokavidya/api/feedbacks";
	var settings = {
		"url": "http://ruralict.cse.iitb.ac.in/lokavidya/api/feedbacks",
  		"method": "POST",
  		"headers": {
    		"x-auth-token": "user:1463462842841:94458b80a8879fdd758d13d0f605338b",
   			"cache-control": "no-cache",
   			"postman-token": "5c511468-4e16-9711-a4c9-eded372bb7ef"
  		}
	}
	$http.post(url,json,settings).success( function(response) {
    	console.log("submitted successfully");
    });

}
});