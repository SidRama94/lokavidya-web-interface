var app=angular.module('lokavidya-app',['ngRoute']);
app.config(function($routeProvider){
  $routeProvider
  .when('/',{
    templateUrl: 'html/home.html'
  })
  .when('/upload',{
    templateUrl: 'html/upload.html'
  })
  .when('/hyperlink', {
    templateUrl: 'html/hyperlink.html'
  })
  .when('/view',{
    templateUrl: 'html/view.html'
  })
  .when('/feedback', {
    templateUrl: 'html/feedback.html'
  })
  .when('/test', {
    templateUrl: 'html/test.html'
  })
});
 /*      
    Here you can handle controller for specific route as well.
    */
    app.controller('cfgController',function($scope){

     
    });