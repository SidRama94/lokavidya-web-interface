var app=angular.module('lokavidya-app',['ngRoute']);
app.config(function($routeProvider){
  $routeProvider
  .when('/',{
    templateUrl: '/home.html'
  })
  .when('/upload',{
    templateUrl: '/upload.html'
  })
  .when('/hyperlink', {
    templateUrl: '/hyperlink.html'
  })
  .when('/view',{
    templateUrl: '/view.html'
  })
  .when('/feedback', {
    templateUrl: '/feedback.html'
  })
  .when('/test', {
    templateUrl: '/test.html'
  })
});
 /*      
    Here you can handle controller for specific route as well.
    */
    app.controller('cfgController',function($scope){

     
    });